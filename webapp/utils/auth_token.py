from functools import wraps
import jwt
from flask import request, jsonify
from webapp.utils import app_config
from webapp.db_service.login_db_service import login_token_data
from webapp.utils.response_helper import ResponseHelper
from logger_config import MyLogger

"""
auth_token use for JWT authentication use case
"""

logger = MyLogger().get_logger("auth_token_logger")


def token_required(f):
    """
    use when you want a JWT authentication

    :return: decorated function
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        """
        x-access-token request get a token in encoded form in this function
        encode the token and check in login table >> login_id and give a
        function return or give invalid token.
        :return: json response of token status.
        """

        token = None

        # search for x-access-token if get >>> token value assign
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        # if not found token
        if not token:
            message = 'A token is missing'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of token_required token"
                        " {}".format(res))
            return jsonify(res), status_code

        try:
            # decode >> token, secret_key
            data = jwt.decode(token, app_config.secret_key,
                              algorithms=[app_config.token_algorithm])

            # decode data search in database
            current_user = login_token_data(data)

            # if user search throw an error
            if not current_user:
                message = 'login_token_data search service error'
                status_code = 500
                res = ResponseHelper.generate_payload(status_code, False,
                                                      message)
                logger.info("--final response of token_required token"
                            "{}".format(res))
                return jsonify(res), status_code

            return f(current_user, *args, **kwargs)

        except Exception as token_invalid:
            # token input invalid
            message = 'token is invalid'
            status_code = 400
            logger.error(f'token is invalid {token_invalid}')
            res = ResponseHelper.generate_payload(status_code, False, message)

            logger.info("--final response of token_required API {}".format(
                res))
            return jsonify(res), status_code

    return decorated

