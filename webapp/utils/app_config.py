from configparser import ConfigParser

config = ConfigParser()

config.read('/home/dev6/PycharmProjects'
            '/Task_Flask/project_demo/webapp/config/dev_config.ini')


data_base = config.get('flask', 'database')
secret_key = config.get('flask', 'secret_key')
debug = config.getboolean('flask', 'debug')
port = config.getint('flask', 'port')
threaded = config.getboolean('flask', 'threaded')
token_expire = config.getint('jwt', 'token_expiration_time')
token_algorithm = config.get('jwt', 'token_algorithm')

