from logger_config import MyLogger

"""
response helper is help give function response in json format 
"""

logger = MyLogger().get_logger("response helper")


class ResponseHelper:

    @staticmethod
    def generate_payload(status_code, success, message, data: dict = None):
        """
        a generate payload help for response and return value in dictionary.

        :param status_code: integer
        :param success: bool
        :param message: string
        :param data: dictionary

        :return: response dictionary

        """
        try:
            logger.info('generate payload success')
            return {
                'status_code': status_code,
                'success': success,
                'message': message,
                'data': data
            }

        except Exception as error:
            logger.info(f'generate payload error {error}')
            return {
                'status_code': None,
                'success': None,
                'message': None,
                'data': None
            }

