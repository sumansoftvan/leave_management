from webapp import db

"""
login details model use for create a login details table in database and from 
query get value and store in model if not table in database it create if exist 
already extend model
"""


class LoginDetailsModel(db.Model):
    __tablename__ = 'login_details_table'
    __table_args__ = {'extend_existing': True}

    login_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    login_username = db.Column(db.String(100), nullable=False)
    login_password = db.Column(db.String(255), nullable=False)
    login_roll = db.Column(db.String(50), nullable=False, default="user")
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_on = db.Column(db.String(50), nullable=False)
    modified_on = db.Column(db.String(50), nullable=False)

    @staticmethod
    def login__details_dict(self):
        """

            :param self: object of login model
            :return: login data dictionary

        """

        return {
            'login_username': self.login_username,
            'login_admin': self.login_roll
        }


db.create_all()
