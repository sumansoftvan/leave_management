from datetime import datetime

from webapp import db
from webapp.models.user_details_model import UserDetailsModel

"""
Leave Request model use for create a Leave Request table in database and from 
database query get value and store in model if not table in database it 
create if exist already extend model
"""


class LeaveRequestModel(db.Model):
    __tablename__ = 'leave_request_table'
    __table_args__ = {'extend_existing': True}

    leave_request_id = db.Column(db.Integer, primary_key=True,
                                 autoincrement=True)
    user_full_name_user_id = db.Column(db.Integer,
                                       db.ForeignKey(UserDetailsModel.user_id,
                                                     ondelete='CASCADE',
                                                     onupdate='CASCADE'))
    leave_apply_to_admin_id = db.Column(db.Integer,
                                        db.ForeignKey(UserDetailsModel.user_id,
                                                      ondelete='CASCADE',
                                                      onupdate='CASCADE'))
    leave_form_date = db.Column(db.Date, nullable=False)
    leave_to_date = db.Column(db.Date, nullable=False)
    leave_reason = db.Column(db.String(255), nullable=False)
    leave_status = db.Column(db.String(255), nullable=False, default="Pending")
    leave_apply_date = db.Column(db.String(255), nullable=False,
                                 default=datetime.now())
    is_active = db.Column(db.Boolean, nullable=False, default=True)

    @staticmethod
    def leave_request_dict(self):
        """

            :param self: object of leave request model
            :return: leave Request data dictionary

        """

        return {
            'user_full_name_user_id': self.user_full_name_user_id,
            'leave_apply_to_admin_id': self.leave_apply_to_admin_id,
            'leave_form_date': self.leave_form_date,
            'leave_to_date': self.leave_to_date,
            'leave_reason': self.leave_reason
        }


db.create_all()
