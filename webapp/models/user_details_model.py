from datetime import datetime

from webapp import db
from webapp.models.login_details_model import LoginDetailsModel

"""
user Details model use for create a user Details table in database and from 
database query get value and store in model if not table in database it create
if exist already extend model
"""


class UserDetailsModel(db.Model):
    __tablename__ = 'user_details_table'
    __table_args__ = {'extend_existing': True}

    user_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_login_id = db.Column(db.Integer,
                              db.ForeignKey(LoginDetailsModel.login_id,
                                            ondelete='CASCADE',
                                            onupdate='CASCADE'))
    user_full_name = db.Column(db.String(100), nullable=False)
    user_mobile_number = db.Column(db.String(50), nullable=False)
    user_address = db.Column(db.String(100), nullable=False)
    user_gender = db.Column(db.String(100), nullable=False)
    user_birthdate = db.Column(db.String(100), nullable=False)
    user_join_date = db.Column(db.String(100), nullable=False)
    user_cl = db.Column(db.Integer, nullable=False)
    user_pl = db.Column(db.Integer, nullable=False)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_on = db.Column(db.String(50), nullable=False)
    modified_on = db.Column(db.String(50), nullable=False)

    @staticmethod
    def user_details_dict(self):
        """
            :param self: object of user model
            :return: user data dictionary

        """

        return {
            'user_full_name': self.user_full_name,
            'user_mobile_number': self.user_mobile_number,
            'user_address': self.user_address,
            'user_gender': self.user_gender,
            'user_birthdate': self.user_birthdate,
            'user_join_date': self.user_join_date,
            'user_cl': self.user_cl,
            'user_pl': self.user_pl

        }


db.create_all()
