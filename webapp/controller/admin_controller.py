from flask import request, jsonify

from logger_config import MyLogger
from webapp import app
from webapp.db_service.Leave_db_service import \
    leave_appy_to_search_service, leave_check_leave_id, \
    leave_delete_service
from webapp.db_service.login_db_service import login_all_admin_search_service
from webapp.db_service.user_db_service import user_search_service, \
    user_full_name_service_user_id, user_update_service, \
    user_check_user_full_name, user_cl_pl_get
from webapp.models.leave_request_model import LeaveRequestModel
from webapp.models.user_details_model import UserDetailsModel
from webapp.utils.response_helper import ResponseHelper

logger = MyLogger().get_logger("auth_logger")


@app.route('/api/v1/admin-view-leave-search', methods=['POST'])
def admin_view_leave_search():
    """
    category-search api is use for search all data from data base for
    authorize user.

    :return: json response of API status.
    """

    logger.info("{}".format(admin_view_leave_search.__doc__))

    try:
        data = request.get_json()
        admin_full_name = data["fullName"]
        admin_user_details = user_check_user_full_name(admin_full_name)
        admin_user_id = admin_user_details.user_id
        leave_data = leave_appy_to_search_service(admin_user_id)

        # if all category data search throw an error
        if leave_data is None:
            message = 'category data search service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of leave_search API {}".format(
                res))
            return jsonify(res), status_code

        # if user category data not in database >> get empty list
        if not leave_data:
            message = 'leave_search_service data search no data found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, True, message)

        if leave_data:
            list_data = []
            for leave_search_details in leave_data:
                user_user_id = leave_search_details.user_full_name_user_id
                user_name = user_cl_pl_get(user_user_id)

                leave_request_id = leave_search_details.leave_request_id
                user_full_name = user_name.user_full_name
                leave_form_date = leave_search_details.leave_form_date
                leave_to_date = leave_search_details.leave_to_date
                leave_reason = leave_search_details.leave_reason
                leave_status = leave_search_details.leave_status

                data = {'leave_request_id': leave_request_id,
                        'user_full_name': user_full_name,
                        'leave_to_date': leave_to_date,
                        'leave_form_date': leave_form_date,
                        'leave_reason': leave_reason,
                        'leave_status': leave_status}
                list_data.append(data)

            message = 'your leave data search successfully.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  list_data)

    except Exception as CategorySearchError:
        message = 'category data search server error.'
        status_code = 500
        logger.error(f'category_search server error{CategorySearchError}')
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of category_search API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/all-Admin-search', methods=['POST'])
def all_admin_search():
    """
    user-search api is use for search all data from data base.

    :return: json response of API status.
    """

    logger.info("{}".format(all_admin_search.__doc__))

    try:
        data = login_all_admin_search_service()

        if data is None:
            message = 'all admin data search service error.'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.error(
                "--final response of all_admin_search API {}".format(res))
            return jsonify(res), status_code

        if not data:
            message = 'all_admin_search not found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, True, message)

        # all user data get in list >>> store in dic. wih for loop
        if data:
            list_data = []
            for user in data:
                user_login_id = user.login_id
                user_data = user_search_service(user_login_id)
                admin_name = user_data.user_full_name
                data = {'admin_name': admin_name}
                list_data.append(data)

            message = 'successfully search data.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  list_data)

    except Exception as UserSearchError:
        message = 'user_search_server error'
        status_code = 500
        logger.error('user_search_server error {}'.format(UserSearchError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.error("--final response of user_search API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/leave-accept/<leave_id>', methods=['GET'])
def leave_accept(leave_id):
    """
        user-delete api is use for user soft delete from data base. means user
        active status change to false from True.

        :param: user login id
        :type: integer
        :return: json response of API status.
        """
    logger.info("{}".format(leave_accept.__doc__))

    try:
        user = leave_check_leave_id(leave_id)

        # if not found a login_id
        if user is None:
            message = 'user data not found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_delete API {}".format(res))
            return jsonify(res), status_code

        # if found a login_id >>> for delete user >> is_active status change
        # True to False in both user and login table
        if user:
            leave_model = LeaveRequestModel()
            leave_model.leave_request_id = leave_id
            leave_model.leave_status = "Accepted"

            leave_delete_service(leave_model)

            leave_details = leave_check_leave_id(leave_id)
            user_id = leave_details.user_full_name_user_id

            user_details = user_full_name_service_user_id(user_id)
            current_cl = user_details.user_cl
            current_pl = user_details.user_pl

            if current_cl > 0:
                cl_cut = UserDetailsModel()
                cl_cut.user_id = user_details.user_id
                cl_cut.user_cl = current_cl - 1
                user_update_service(cl_cut)
            if current_cl <= 0:
                pl_cut = UserDetailsModel()
                pl_cut.user_id = user_details.user_id
                pl_cut.user_pl = current_pl - 1
                user_update_service(pl_cut)

            message = 'successfully data Accepted.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  {'login_id': leave_id})
        # if login_id search throw an error
        if not user:
            message = 'user data delete service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)
    except Exception as UserDeleteError:
        message = 'user_delete_server error'
        status_code = 500
        logger.error('user_delete server_error - {}'.format(UserDeleteError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of user_delete API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/leave-Reject/<leave_id>', methods=['GET'])
def leave_reject(leave_id):
    """
        user-delete api is use for user soft delete from data base. means user
        active status change to false from True.

        :param: user login id
        :type: integer
        :return: json response of API status.
        """
    logger.info("{}".format(leave_reject.__doc__))

    try:

        # check passed id in login table
        user = leave_check_leave_id(leave_id)

        # if not found a login_id
        if user is None:
            message = 'user data not found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_delete API {}".format(res))
            return jsonify(res), status_code

        # if found a login_id >>> for delete user >> is_active status change
        # True to False in both user and login table
        if user:
            leave_model = LeaveRequestModel()
            leave_model.leave_request_id = leave_id
            leave_model.leave_status = "Rejected"

            leave_delete_service(leave_model)

            message = 'successfully data delete.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  {'login_id': leave_id})

        # if login_id search throw an error
        if not user:
            message = 'user data delete service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)
    except Exception as UserDeleteError:
        message = 'user_delete_server error'
        status_code = 500
        logger.error('user_delete server_error - {}'.format(UserDeleteError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of user_delete API {}".format(res))
    return jsonify(res), status_code
