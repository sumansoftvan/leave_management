from flask import request, jsonify

from logger_config import MyLogger
from webapp import app
from webapp.db_service.Leave_db_service import leave_search_service, \
    leave_insert_service, leave_check_leave_id, leave_delete_service, \
    leave_search_dash_service
from webapp.db_service.user_db_service import user_check_user_full_name,\
    user_full_name_service_user_id
from webapp.models.leave_request_model import LeaveRequestModel
from webapp.utils.response_helper import ResponseHelper

logger = MyLogger().get_logger("leave_apply_logger")


@app.route('/api/v1/leave-add', methods=['POST'])
def leave_create():
    """
    after login user have authorization to add data in category table.

    :type: string.
    :return: json response of API status.
    """

    logger.info("{}".format(leave_create.__doc__))

    try:
        data = request.get_json()
        from_date = data['fromDate']
        to_date = data['toDate']
        reason = data['Reason']
        apply_to = data['applyTo']
        user_full_name = data['fullName']

        # check input category_name not empty
        if not from_date:
            message = 'from_date is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of category_create API "
                        "{}".format(res))
            return jsonify(res), status_code

        # check input category_description not empty
        if not to_date:
            message = 'to_date is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of category_create API "
                        "{}".format(res))
            return jsonify(res), status_code

        if not reason:
            message = 'reason is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of category_create API "
                        "{}".format(res))
            return jsonify(res), status_code

        if not apply_to:
            message = 'apply_to is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of category_create API "
                        "{}".format(res))
            return jsonify(res), status_code

        user_details = user_check_user_full_name(user_full_name)
        admin_details = user_check_user_full_name(apply_to)

        leave_object = LeaveRequestModel()
        leave_object.leave_form_date = from_date
        leave_object.leave_to_date = to_date
        leave_object.leave_reason = reason
        leave_object.user_full_name_user_id = user_details.user_id
        leave_object.leave_apply_to_admin_id = admin_details.user_id

        leave_insert_service(leave_object)

        status_code = 200
        message = 'new leave add.'
        res = ResponseHelper.generate_payload(status_code, True, message, data)
    except Exception as CategoryInsertError:
        message = 'category_insert server error'
        status_code = 500
        logger.error('category insert error {}'.format(CategoryInsertError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of category_create API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/leave-search', methods=['POST'])
def leave_search():
    """
    leave-search api is use for search all leave from data base for one user .

    :return: json response of API status.
    """

    logger.info("{}".format(leave_search.__doc__))

    try:
        data = request.get_json()

        user_full_name = data["fullName"]
        user_details = user_check_user_full_name(user_full_name)
        user_id = user_details.user_id
        data = leave_search_service(user_id)

        if data is None:
            message = 'leave data search service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of leave_search API {}".format(
                res))
            return jsonify(res), status_code

        if not data:
            message = 'leave_search_service data search no data found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, True, message)

        if data:
            admin_id = data[0].leave_apply_to_admin_id
            admin_details = user_full_name_service_user_id(admin_id)
            list_data = []
            for leave_search_details in data:
                leave_request_id = leave_search_details.leave_request_id
                full_name = user_details.user_full_name
                leave_form_date = leave_search_details.leave_form_date
                leave_to_date = leave_search_details.leave_to_date
                leave_apply_to = admin_details.user_full_name
                leave_reason = leave_search_details.leave_reason
                leave_status = leave_search_details.leave_status

                data = {'leave_request_id': leave_request_id,
                        'full_name': full_name,
                        'leave_form_date': leave_form_date,
                        'leave_to_date': leave_to_date,
                        'leave_apply_to': leave_apply_to,
                        'leave_reason': leave_reason,
                        'leave_status': leave_status}
                list_data.append(data)

            message = 'your category data search successfully.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  list_data)
    except Exception as CategorySearchError:
        message = 'category data search server error.'
        status_code = 500
        logger.error(f'category_search server error{CategorySearchError}')
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of category_search API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/leave-delete/<leave_request_id>', methods=['GET'])
def leave_delete(leave_request_id):
    """
        leave-delete api is use for user soft delete from data base. means
        leave
        active status change to false from True.

        :param: leave id
        :type: integer
        :return: json response of API status.
        """
    logger.info("{}".format(leave_delete.__doc__))

    try:
        user = leave_check_leave_id(leave_request_id)

        if user is None:
            message = 'leave data not found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_delete API {}".format(res))
            return jsonify(res), status_code

        if user:
            leave_model = LeaveRequestModel()
            leave_model.leave_request_id = leave_request_id
            leave_model.is_active = False

            leave_delete_service(leave_model)

            message = 'successfully leave delete.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  {'leave_request_id':
                                                       leave_request_id})
        if not user:
            message = 'leave data delete service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)

    except Exception as UserDeleteError:
        message = 'user_delete_server error'
        status_code = 500
        logger.error('leave_delete server_error - {}'.format(UserDeleteError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of leave_delete API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/leave-search-date', methods=['POST'])
def leave_search_conform():
    """
    leave-search api is use for search all data from data base for
    authorize user.

    :return: json response of API status.
    """

    logger.info("{}".format(leave_search_conform.__doc__))

    try:
        data = leave_search_dash_service()

        if data is None:
            message = 'leave data search service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of leave_search API {}".format(
                res))
            return jsonify(res), status_code

        if not data:
            message = 'leave_search_service data search no data found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, True, message)

        if data:
            list_data = []
            for leave_search_details in data:
                user_id = leave_search_details.user_full_name_user_id
                user_details = user_full_name_service_user_id(user_id)
                full_name = user_details.user_full_name
                leave_form_date = leave_search_details.leave_form_date
                leave_to_date = leave_search_details.leave_to_date

                data = {
                    'title': full_name,
                    'leave_form_date': leave_form_date,
                    'leave_to_date': leave_to_date
                }
                list_data.append(data)

            message = 'your leave data search successfully.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  list_data)

    except Exception as CategorySearchError:
        message = 'category data search server error.'
        status_code = 500
        logger.error(f'category_search server error{CategorySearchError}')
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of category_search API {}".format(res))
    return jsonify(res), status_code
