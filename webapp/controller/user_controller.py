import re
from datetime import datetime

import bcrypt
from flask import request, jsonify

from logger_config import MyLogger
from webapp import app
from webapp.db_service.login_db_service import \
    login_details_insert_service, login_delete_service, \
    login_search_service, login_update_service, login_check_login_user_name
from webapp.db_service.user_db_service import user_login_use_check, \
    user_details_insert_service, \
    user_delete_service, user_update_service, \
    user_search_service, user_active_search_service, \
    user_check_user_full_name, user_name_login_use_check, \
    user_login_use_check_new_user
from webapp.models.login_details_model import LoginDetailsModel
from webapp.models.user_details_model import UserDetailsModel
from webapp.utils.response_helper import ResponseHelper

"""user controller use for user request response handler in user_controller"""

rex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
pattern = r'^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])' \
          '(?=.*[@#$%^&+=]).*$'
logger = MyLogger().get_logger("user_logger")


@app.route('/api/v1/user-new-register', methods=["POST"])
def user_new_register():
    """user_register api is use for new use registration if user has already
        registered before it gives message to user that you have already
        registered, once user has use a username again user can't use that
        user name he has to add new username, when user register by default
        user in user mode.

        :parameter: data in Json
        :type: Json
        :return: json response of API status.
        """

    logger.info("{}".format(user_new_register.__doc__))

    try:
        data = request.get_json()

        user_full_name = data["full_name"]
        login_username = data['email']
        login_password = data['password']
        user_mobile_number = data['mobileNumber']
        user_address = data['address']
        user_gender = data['gender']
        user_birthdate = data['birthDate']
        user_join_date = data['joinDate']
        cl = data['cl']
        pl = data['pl']

        # check input user_full_name not empty
        if not user_full_name:
            message = 'full name is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input login_username not empty
        if not login_username:
            message = 'Email is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input user_mobile_number not empty
        if not user_mobile_number:
            message = 'mobile number is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input user_address not empty
        if not user_address:
            message = 'Address is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input login_password not empty
        if not login_password:
            message = 'login_password is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input user_gender not empty
        if not user_gender:
            message = 'Gender is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input user_birthdate not empty
        if not user_birthdate:
            message = 'Birthdate is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input user_join_date not empty
        if not user_join_date:
            message = 'Join Date is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input cl not empty
        if not cl:
            message = 'cl is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input pl not empty
        if not pl:
            message = 'pl is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # validation of login_username
        if not re.fullmatch(rex, login_username):
            message = 'enter valid username.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # validation of login_password >>> input strong password
        if not re.findall(pattern, login_password):
            message = 'enter valid password.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check user_name & Full_name has already register or not
        user = user_login_use_check_new_user(login_username, user_full_name)

        # for user registration if user has not registered before
        if user is None:
            login_details_model = LoginDetailsModel()
            login_details_model.login_username = login_username
            login_details_model.login_password = bcrypt.hashpw(
                login_password.encode('utf8'), bcrypt.gensalt())
            login_details_model.created_on = datetime.now()
            login_details_model.modified_on = login_details_model.created_on

            user_login_id = login_details_insert_service(login_details_model)

            user_details_model = UserDetailsModel()
            user_details_model.user_login_id = user_login_id
            user_details_model.user_full_name = user_full_name
            user_details_model.user_mobile_number = user_mobile_number
            user_details_model.user_address = user_address
            user_details_model.user_gender = user_gender
            user_details_model.user_birthdate = user_birthdate
            user_details_model.user_join_date = user_join_date
            user_details_model.user_cl = cl
            user_details_model.user_pl = pl
            user_details_model.created_on = login_details_model.created_on
            user_details_model.modified_on = login_details_model.modified_on

            user_details_insert_service(user_details_model)

            # user and login data get in dic. and merge two dic.
            data_user = user_details_model.user_details_dict(
                user_details_model)
            data_login = login_details_model.login__details_dict(
                login_details_model)
            data_user.update(data_login)

            message = 'successfully register new user.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  data_user)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # if user already registered
        if user == 'login_user_name Already use.':
            message = 'login_user_name Already use.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False,
                                                  message)
            logger.info(
                "--final response of user_update API {}".format(res))
            return jsonify(res), status_code

        if user == 'user_full_name Already use.':
            message = 'user_full_name Already use.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False,
                                                  message)
            logger.info(
                "--final response of user_update API {}".format(res))
            return jsonify(res), status_code

        # if user table search throw an error
        if not user:
            message = 'registered user_login_use_check check service error.'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)

    except Exception as RegisterError:
        message = 'register server error.'
        status_code = 500
        logger.error('user_new_register server error - {}'.format(
            RegisterError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of user_register API {}".format(
        res))
    return jsonify(res), status_code


@app.route('/api/v1/user-search', methods=['POST'])
def user_search():
    """
    user-search api is use for search all data from data base
    (active status true).

    :return: json response of API status.
    """

    logger.info("{}".format(user_search.__doc__))

    try:
        # all user data search >>> get all data in list
        data = user_active_search_service()

        # if all user data search throw an error
        if data is None:
            message = 'user data search service error.'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.error("--final response of user_search API {}".format(res))
            return jsonify(res), status_code

        # if user data not in database >> get empty list
        if not data:
            message = 'user data not found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, True, message)

        # all user data get in list >>> store in dic. wih for loop
        if data:
            list_data = []
            for user in data:
                user_id = user.user_id
                user_login_id = user.user_login_id

                login_data = login_search_service(user_login_id)
                if login_data.login_roll == "superAdmin":
                    continue

                login_username = login_data.login_username
                login_password = login_data.login_password
                login_roll = login_data.login_roll
                admin = login_data.login_roll

                user_full_name = user.user_full_name
                user_mobile_number = user.user_mobile_number
                user_address = user.user_address
                user_gender = user.user_gender
                user_birthdate = user.user_birthdate
                user_join_date = user.user_join_date
                user_cl = user.user_cl
                user_pl = user.user_pl
                created_on = user.created_on
                modified_on = user.modified_on
                data = {'user_id': user_id,
                        'user_login_id': user_login_id,
                        'user_full_name': user_full_name,
                        'login_username': login_username,
                        'login_password': login_password,
                        'user_mobile_number': user_mobile_number,
                        'user_cl': user_cl,
                        'user_pl': user_pl,
                        'login_roll': login_roll,
                        'user_address': user_address,
                        'user_gender': user_gender,
                        'user_birthdate': user_birthdate,
                        'user_join_date': user_join_date,
                        'admin': admin,
                        'created_on': created_on,
                        'modified_on': modified_on}

                list_data.append(data)

            message = 'successfully search data.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  list_data)

    except Exception as UserSearchError:
        message = 'user_search_server error'
        status_code = 500
        logger.error('user_search_server error {}'.format(UserSearchError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.error("--final response of user_search API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/user-delete/<login_id>', methods=['GET'])
def user_delete(login_id):
    """
        user-delete api is use for user soft delete from data base. means user
        active status change to false from True.

        :param: user login id
        :type: integer
        :return: json response of API status.
        """
    logger.info("{}".format(user_delete.__doc__))

    try:
        # check passed id in login table
        user = login_search_service(login_id)

        # if not found a login_id
        if user is None:
            message = 'user data not found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_delete API {}".format(res))
            return jsonify(res), status_code

        # if found a login_id >>> for delete user >> is_active status change
        # True to False in both user and login table
        if user:
            login = LoginDetailsModel()
            login.login_id = login_id
            login.is_active = False
            login.modified_on = datetime.now()

            login_delete_service(login)

            user = UserDetailsModel()
            user_detail = user_search_service(login_id)
            user.user_id = user_detail.user_id
            user.is_active = False
            user.modified_on = datetime.now()

            user_delete_service(user)

            message = 'successfully data delete.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  {'login_id': login_id})

        if not user:
            message = 'user data delete service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)

    except Exception as UserDeleteError:
        message = 'user_delete_server error'
        status_code = 500
        logger.error('user_delete server_error - {}'.format(UserDeleteError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of user_delete API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/user-details/<user_login_id>', methods=['GET'])
def user_details(user_login_id):
    """
        user details api is use for get details of user.

        :param : login_id
        :type: integer
        :return: json response of API status.
        """

    logger.info("{}".format(user_details.__doc__))

    try:
        user_login_model = login_search_service(user_login_id)
        user_user_model = user_search_service(user_login_model.login_id)

        if user_login_model is None:
            message = 'wrong input data not found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_details API {}".format(res))
            return jsonify(res), status_code

        if user_login_model:
            full_name = user_user_model.user_full_name
            username = user_login_model.login_username
            mobile_number = user_user_model.user_mobile_number
            address = user_user_model.user_address
            gender = user_user_model.user_gender
            birthday = user_user_model.user_birthdate
            cl = user_user_model.user_cl
            pl = user_user_model.user_pl
            join_date = user_user_model.user_join_date
            created_on = user_user_model.created_on
            login_roll = user_login_model.login_roll

            data = {'full_name': full_name,
                    'username': username,
                    'mobile_number': mobile_number,
                    'address': address,
                    'gender': gender,
                    'birthday': birthday,
                    'join_date': join_date,
                    'created_on': created_on,
                    'login_roll': login_roll,
                    'cl': cl,
                    'pl': pl
                    }

            message = 'your data successfully Get.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  data)

        if not user_login_model:
            message = 'user data update service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)

        if not user_user_model:
            message = 'user data update service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)

    except Exception as UserUpdateError:
        message = 'user data details server error.'
        status_code = 500
        logger.error('user_data_details_error {}'.format(UserUpdateError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of user_update API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/user-update', methods=['POST'])
def user_update():
    """
        user update api is use for user can update data in user table.

        :param : json
        :type: integer
        :return: json response of API status.
        """

    logger.info("{}".format(user_update.__doc__))

    try:
        data = request.get_json()

        edit_id = data["editId"]
        user_full_name = data["full_name"]
        user_mobile_number = data['mobileNumber']
        user_address = data['address']
        user_gender = data['gender']
        user_birthdate = data['birthDate']
        user_join_date = data['joinDate']
        user_cl = data['cl']
        user_pl = data['pl']
        login_username = data['email']
        login_password = data['password']

        # check input user_firstname not empty
        if not user_full_name:
            message = 'full name is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input login_username not empty
        if not login_username:
            message = 'Email is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input user_lastname not empty
        if not user_mobile_number:
            message = 'mobile number is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not user_address:
            message = 'Address is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not login_password:
            message = 'login_password is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not user_gender:
            message = 'Gender is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not user_birthdate:
            message = 'Birthdate is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not user_join_date:
            message = 'Join Date is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not user_cl:
            message = 'cl is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not user_pl:
            message = 'pl is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # validation of login_username
        if not re.fullmatch(rex, login_username):
            message = 'enter valid username.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # validation of login_password >>> input strong password
        if not re.findall(pattern, login_password):
            message = 'enter valid password.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        user_login_model = login_search_service(edit_id)
        user_user_model = user_search_service(edit_id)

        # if user update as login id not found in database
        if user_login_model is None and user_user_model is None:
            message = 'wrong input data not found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_update API {}".format(res))
            return jsonify(res), status_code

        # if user promote id found in database and change >> user to admin
        if user_login_model and user_user_model:
            user_user_name_use = None
            user_full_name_use = None

            # check user_name & Full_name has already register or not
            if login_username != user_login_model.login_username:
                user_user_name_use = user_login_use_check(login_username)

            if user_full_name != user_user_model.user_full_name:
                user_full_name_use = user_name_login_use_check(user_full_name)

            if user_user_name_use == 'login_user_name Already use.':
                message = 'login_user_name Already use.'
                status_code = 400
                res = ResponseHelper.generate_payload(status_code, False,
                                                      message)
                logger.info(
                    "--final response of user_update API {}".format(res))
                return jsonify(res), status_code

            if user_full_name_use == 'user_full_name Already use.':
                message = 'user_full_name Already use.'
                status_code = 400
                res = ResponseHelper.generate_payload(status_code, False,
                                                      message)
                logger.info(
                    "--final response of user_update API {}".format(res))
                return jsonify(res), status_code

            login_model = LoginDetailsModel()
            login_model.login_id = edit_id
            login_model.login_username = login_username

            if login_password == user_login_model.login_password:
                login_model.login_password = user_login_model.login_password
            if login_password != user_login_model.login_password:
                login_model.login_password = bcrypt.hashpw(
                    login_password.encode('utf8'), bcrypt.gensalt())
            login_model.modified_on = datetime.now()

            login_update_service(login_model)

            user_model = UserDetailsModel()
            user_model.user_id = user_user_model.user_id
            user_model.user_full_name = user_full_name
            user_model.user_mobile_number = user_mobile_number
            user_model.user_address = user_address
            user_model.user_gender = user_gender
            user_model.user_cl = user_cl
            user_model.user_pl = user_pl
            user_model.user_birthdate = user_birthdate
            user_model.user_join_date = user_join_date
            user_model.modified_on = datetime.now()

            user_update_service(user_model)

            message = 'your data successfully update'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  {'login_id': edit_id})

        if not user_login_model:
            message = 'user data update service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)

        if not user_user_model:
            message = 'user data update service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)

    except Exception as UserUpdateError:
        message = 'user data update server error.'
        status_code = 500
        logger.error('user_data_update_error {}'.format(UserUpdateError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of user_update API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/user-profile-update', methods=['POST'])
def user_profile_update():
    """
        user update api is use for user can update data in user table.

        :param : login_id
        :type: integer
        :return: json response of API status.
        """

    logger.info("{}".format(user_profile_update.__doc__))

    try:
        data = request.get_json()
        current_user_name = data["current_userName"]
        current_full_name = data["current_fullName"]
        user_full_name = data["name"]
        user_mobile_number = data['mobileNumber']
        user_address = data['address']
        user_gender = data['gender']
        user_birthdate = data['birthDate']
        login_username = data['email']
        login_password = data['password']

        if not current_user_name:
            message = 'current_user_name is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not current_full_name:
            message = 'current_full_name is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input user_firstname not empty
        if not user_full_name:
            message = 'full name is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input login_username not empty
        if not login_username:
            message = 'Email is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # check input user_lastname not empty
        if not user_mobile_number:
            message = 'mobile number is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not user_address:
            message = 'Address is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not login_password:
            message = 'login_password is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not user_gender:
            message = 'Gender is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        if not user_birthdate:
            message = 'Birthdate is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # validation of login_username
        if not re.fullmatch(rex, login_username):
            message = 'enter valid username.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        # validation of login_password >>> input strong password
        if not re.findall(pattern, login_password):
            message = 'enter valid password.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_register API {}".format(
                res))
            return jsonify(res), status_code

        user_login_model = login_check_login_user_name(current_user_name)
        user_user_model = user_check_user_full_name(current_full_name)

        # if user update as login id not found in database
        if user_login_model is None and user_user_model is None:
            message = 'wrong input data not found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_update API {}".format(res))
            return jsonify(res), status_code

        if user_login_model and user_user_model:
            user_full_name_use = None
            user_user_name_use = None

            if login_username != user_login_model.login_username:
                user_user_name_use = user_login_use_check(login_username)

            if user_full_name != user_user_model.user_full_name:
                user_full_name_use = user_name_login_use_check(user_full_name)

            if user_user_name_use == 'login_user_name Already use.':
                message = 'login_user_name Already use.'
                status_code = 400
                res = ResponseHelper.generate_payload(status_code, False,
                                                      message)
                logger.info(
                    "--final response of user_update API {}".format(res))
                return jsonify(res), status_code

            if user_full_name_use == 'user_full_name Already use.':
                message = 'user_full_name Already use.'
                status_code = 400
                res = ResponseHelper.generate_payload(status_code, False,
                                                      message)
                logger.info(
                    "--final response of user_update API {}".format(res))
                return jsonify(res), status_code

            login_model = LoginDetailsModel()
            login_model.login_id = user_login_model.login_id

            login_model.login_username = login_username
            login_model.modified_on = datetime.now()

            if login_password == user_login_model.login_password:
                login_model.login_password = user_login_model.login_password

            if login_password != user_login_model.login_password:
                login_model.login_password = bcrypt.hashpw(
                    login_password.encode('utf8'), bcrypt.gensalt())

            login_update_service(login_model)

            user_model = UserDetailsModel()
            user_model.user_id = user_user_model.user_id
            user_model.user_mobile_number = user_mobile_number
            user_model.user_address = user_address
            user_model.user_gender = user_gender
            user_model.user_birthdate = user_birthdate
            user_model.modified_on = login_model.modified_on
            user_model.user_full_name = user_full_name

            user_update_service(user_model)

            message = 'your data successfully update'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  {'user_full_name':
                                                       user_full_name})

        if not user_login_model:
            message = 'user data update service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)

        if not user_user_model:
            message = 'user data update service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)

    except Exception as UserUpdateError:
        message = 'user data update server error.'
        status_code = 500
        logger.error('user_data_update_error {}'.format(UserUpdateError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of user_update API {}".format(res))
    return jsonify(res), status_code
