from datetime import datetime, timedelta

import bcrypt
import jwt
from flask import request, jsonify

from logger_config import MyLogger
from webapp import app
from webapp.db_service.login_db_service import \
    login_check_login_id_promote_user, \
    login_update_service, login_check_login_user_name
from webapp.db_service.user_db_service import user_full_name_service
from webapp.models.login_details_model import LoginDetailsModel
from webapp.utils import app_config
from webapp.utils.response_helper import ResponseHelper

"""
auth controller is use for request response for authentication purpose.
after login user get a JWT token and with JWT token authorize user promote 
user as user to admin.
"""

logger = MyLogger().get_logger("auth_controller_logger")


@app.route('/api/v1/user-login', methods=['POST'])
def user_login():
    """
    user login api is use for user authentication purpose and it give the
    current user and JWT token with expire time.

    :param: login_username, login_password
    :type: string
    :return: json response of API status.
    """

    logger.info("{}".format(user_login.__doc__))

    try:
        data = request.get_json()
        login_username = data["loginName"]
        login_password = data["loginPassword"]

        # check input login_username not empty
        if not login_username:
            message = 'login_username is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_login API {}".format(res))
            return jsonify(res), status_code

        # check input login_password not empty
        if not login_password:
            message = 'login_password is empty.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_login API {}".format(res))
            return jsonify(res), status_code

        # check login_username is in database or not
        login_user = login_check_login_user_name(login_username)

        if login_user is None:
            message = 'you are not registered please register for {}'.format(
                login_username)
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False,
                                                  message)
            logger.info(
                "--final response of user_login API {}".format(res))
            return jsonify(res), status_code

        if not login_user:
            message = 'login user_name check service error for user login'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False,
                                                  message)
            return jsonify(res), status_code

        if login_user:
            if login_user.is_active is False:
                message = 'your are not register please register user ' \
                          'data was deleted for {}.'.format(login_username)
                status_code = 400
                res = ResponseHelper.generate_payload(status_code, False,
                                                      message)
                logger.info("--final response of user_login API {}".format(
                    res))
                return jsonify(res), status_code

            if login_user.is_active is True:
                if login_user.login_roll == "superAdmin":
                    if login_user.login_password != login_password:
                        message = 'your login password is wrong. for ' \
                                  '{}.'.format(login_username)
                        status_code = 400
                        res = ResponseHelper.generate_payload(status_code,
                                                              False,
                                                              message)
                        logger.info(
                            "--final response of user_login API {}".format(
                                res))
                        return jsonify(res), status_code

                    token = jwt.encode({'login_id': login_user.login_id,
                                        'exp': datetime.utcnow() + timedelta
                                        (minutes=app_config.token_expire)},
                                       app.config['SECRET_KEY'])

                    user_full_name_object = user_full_name_service(
                        login_user.login_id)
                    print("112 line print", user_full_name_object)
                    user_full_name = user_full_name_object.user_full_name

                    message = 'successfully login.'
                    status_code = 200
                    res = ResponseHelper.generate_payload(status_code,
                                                          True,
                                                          message,
                                                          {'token': token,
                                                           'login_roll':
                                                               login_user.login_roll,
                                                           'user_name':
                                                               login_username,
                                                           'full_name':
                                                               user_full_name,
                                                           'expires_in':
                                                               app_config.token_expire})
                    return jsonify(res), status_code

                if not bcrypt.checkpw(login_password.encode('utf8'),
                                      login_user.login_password.encode(
                                          'utf8')):
                    message = 'your login password is wrong. for ' \
                              '{}.'.format(login_username)
                    status_code = 400
                    res = ResponseHelper.generate_payload(status_code, False,
                                                          message)
                    logger.info("--final response of user_login API {}".format(
                        res))
                    return jsonify(res), status_code

                token = jwt.encode({'login_id': login_user.login_id,
                                    'exp': datetime.utcnow() + timedelta
                                    (minutes=app_config.token_expire)},
                                   app.config['SECRET_KEY'])

                user_full_name_object = user_full_name_service(
                    login_user.login_id)
                user_full_name = user_full_name_object.user_full_name

                message = 'successfully login.'
                status_code = 200
                res = ResponseHelper.generate_payload(status_code, True,
                                                      message,
                                                      {'token': token,
                                                       'login_roll':
                                                           login_user.login_roll,
                                                       'user_name':
                                                           login_username,
                                                       'full_name':
                                                           user_full_name,
                                                       'expires_in':
                                                           app_config.token_expire})

    except Exception as LoginError:
        message = 'login server error'
        status_code = 500
        logger.error('login error - {}'.format(LoginError))
        res = ResponseHelper.generate_payload(status_code, False, message)

        logger.info("--final response of user_login API {}".format(res))
    return jsonify(res), status_code


@app.route('/api/v1/promote-to-admin/<login_id>', methods=['GET'])
def user_promote_admin(login_id):
    """
        user promote api is use for an super user  can promote user to
        admin from user which means updated data change in login table.

        :param : login_id
        :type: integer
        :return: json response of API status.
        """

    logger.info("{}".format(user_promote_admin.__doc__))

    try:
        # user promote id search in login table
        user = login_check_login_id_promote_user(login_id)

        # if user promote as login id not found in database
        if user is None:
            message = 'wrong input data not found.'
            status_code = 400
            res = ResponseHelper.generate_payload(status_code, False, message)
            logger.info("--final response of user_promote API {}".format(res))
            return jsonify(res), status_code

        # if user promote id found in database and change >> user to admin
        if user:
            login_model = LoginDetailsModel()
            login_model.login_id = login_id

            if user.login_roll == "admin":
                login_model.login_roll = "user"
            if user.login_roll == "user":
                login_model.login_roll = "admin"

            login_model.modified_on = datetime.now()

            login_update_service(login_model)

            message = 'Congress You are Promoted To Admin.'
            status_code = 200
            res = ResponseHelper.generate_payload(status_code, True, message,
                                                  {'login_id': login_id})

        if not user:
            message = 'user data promote service error'
            status_code = 500
            res = ResponseHelper.generate_payload(status_code, False, message)

    except Exception as UserPromoteError:
        message = 'user promotion server error.'
        status_code = 500
        logger.error('user_promote_error {}'.format(UserPromoteError))
        res = ResponseHelper.generate_payload(status_code, False, message)

    logger.info("--final response of user_promote API {}".format(res))
    return jsonify(res), status_code
