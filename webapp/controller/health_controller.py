from flask import jsonify

from logger_config import MyLogger
from webapp import app
from webapp.utils.response_helper import ResponseHelper

"""
health controller is use for health api request response control in controller 
check api working status.
"""

logger = MyLogger().get_logger("health_logger")


@app.route('/api/v1/api1-v1-health', methods=['GET'])
def api_health():
    """
    Health API endpoint for checking project API testing
    :return: json response of API status
    """

    logger.info("{}".format(api_health.__doc__))

    try:
        message = 'health api is working'
        status_code = 200
        res = ResponseHelper.generate_payload(status_code, True, message,
                                              {'api': 'working on'})

    except Exception as error:
        message = 'Health API is not working'
        status_code = 500
        logger.error(f'Error in health API: {error}')
        res = ResponseHelper.generate_payload(status_code, False, message,
                                              {'api': 'working not working'})

    logger.info("--final response of api_health API {}".format(res))
    return jsonify(res), status_code
