from flask import Flask
from flask_cors import CORS
from webapp.utils import app_config
import warnings
from flask_sqlalchemy import SQLAlchemy

warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=DeprecationWarning)

app = Flask(__name__)
CORS(app)

app.config['SECRET_KEY'] = app_config.secret_key
app.config['SQLALCHEMY_DATABASE_URI'] = app_config.data_base

db = SQLAlchemy(app)

from webapp import controller

