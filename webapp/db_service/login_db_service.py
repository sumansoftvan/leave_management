from logger_config import MyLogger
from webapp import db
from webapp.models.login_details_model import LoginDetailsModel

"""login db service gives a data base query regarding to login table " \
"like insert, search, delete and update"""

logger = MyLogger().get_logger("login_db_service_logger")


def login_details_insert_service(login_details_model):
    """
        login details insert service is insert data into login table
        :param login_details_model: login details  object

    """
    logger.info("{}".format(login_details_insert_service.__doc__))

    try:
        db.session.add(login_details_model)
        db.session.commit()

        logger.info('login_details_insert_service data success {}'.format(
            login_details_model))
        return login_details_model.login_id

    except Exception as error:
        logger.error(
            'login_details_insert_service data error {}'.format(error))


def login_search_service(login_id):
    """
        login search service is search all data from login table
        :return: if data in table >>> return data in list,
                 if data not in table >>> return empty list,
                 if error get in search service >>> return None
    """

    logger.info("{}".format(login_search_service.__doc__))

    data = None
    try:
        data = LoginDetailsModel.query.filter(LoginDetailsModel.login_id
                                              == login_id).first()
        logger.info('login data search success {}'.format(data))
        return data

    except Exception as error:
        logger.error('login_search error {}'.format(error))
    return data


def login_all_admin_search_service():
    """
        login search service is search all data from login table
        :return: if data in table >>> return data in list,
                 if data not in table >>> return empty list,
                 if error get in search service >>> return None
    """

    logger.info("{}".format(login_all_admin_search_service.__doc__))

    data = None
    try:
        data = LoginDetailsModel.query.filter(
            LoginDetailsModel.is_active == True,
            LoginDetailsModel.login_roll ==
            "admin").all()
        logger.info('login_all_admin_search_service  success {}'.format(data))
        return data

    except Exception as error:
        logger.error('login_all_admin_search_service error {}'.format(error))
    return data


def login_delete_service(login):
    """
        login delete service is merge data in login table for soft user
        delete in login table.

        :param login: login model object

    """

    logger.info("{}".format(login_delete_service.__doc__))

    try:
        db.session.merge(login)
        db.session.commit()
        logger.info('login_delete success{}'.format(login.login_id))

    except Exception as error:
        logger.error('login_delete error {}'.format(error))


def login_update_service(login_model):
    """
        login update service is merge data in login table for update
        data in login table.

        :param login_model: login model object

    """

    logger.info("{}".format(login_update_service.__doc__))

    try:
        db.session.merge(login_model)
        db.session.commit()
        logger.info('login_update success{}'.format(login_model.login_id))

    except Exception as error:
        logger.error('login_update error {}'.format(error))


def login_check_login_id_promote_user(login_id):
    """
        login_check_login_id is use for check that a login_id is in
        data base or not.

        :param login_id: login_id
        :type: integer
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
    """

    logger.info("{}".format(login_check_login_id_promote_user.__doc__))

    user = []
    try:
        user = LoginDetailsModel.query.filter(
            LoginDetailsModel.login_id == login_id).first()
        logger.info('login_check_login_id_promote_user is {}'.format(user))
    except Exception as error:
        logger.error(
            'login_check_login_id_promote_user error {}'.format(error))
    return user


def login_check_login_user_name(login_username):
    """
        login_check_login_user_name is use for check that a login_username
        is in data base or not.

        :param login_username: login_username
        :type: integer
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
    """

    logger.info("{}".format(login_check_login_user_name.__doc__))

    user = []
    try:
        user = LoginDetailsModel.query.filter(
            LoginDetailsModel.login_username == login_username).first()
        logger.info('login_check_login_user_name is {}'.format(user))
    except Exception as error:
        logger.error('login_check_login_user_name error {}'.format(error))
    return user
