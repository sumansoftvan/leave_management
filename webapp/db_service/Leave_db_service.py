from logger_config import MyLogger
from webapp import db
from webapp.models.leave_request_model import LeaveRequestModel

logger = MyLogger().get_logger("leave_db_service_logger")


def leave_insert_service(leave_data):
    """
    category insert service is insert data into category table
    :param leave_data: leave_data model object

    """
    logger.info("{}".format(leave_insert_service.__doc__))

    try:
        db.session.add(leave_data)
        db.session.commit()
        logger.info('leave_insert_service data success '
                    '{}'.format(leave_data))

    except Exception as error:
        logger.error('leave_insert_service data error {}'.format(error))


def leave_full_name_update_search_service(full_name):
    """
        leave_full_name_update_search_service is search all data from leave
        apply table
        :return: if data in table >>> return data in list,
                 if data not in table >>> return empty list,
                 if error get in search service >>> return None
        """

    logger.info("{}".format(leave_full_name_update_search_service.__doc__))

    data = None
    try:
        user_full_name = LeaveRequestModel.query.filter(
            LeaveRequestModel.user_full_name == full_name).all()
        admin_full_name = LeaveRequestModel.query.filter(
            LeaveRequestModel.leave_apply_to == full_name).all()
        data = [user_full_name, admin_full_name]
        logger.info('leave_full_name_update_search_service search success'
                    ' {}'.format(data))
        return data

    except Exception as error:
        logger.error('leave_full_name_update_search_service error '
                     '{}'.format(error))
    return data


def leave_search_service(user_id):
    """
        leave search service is search all data from leave apply table
        :return: if data in table >>> return data in list,
                 if data not in table >>> return empty list,
                 if error get in search service >>> return None
        """

    logger.info("{}".format(leave_search_service.__doc__))

    data = None
    try:
        data = LeaveRequestModel.query.filter(
            LeaveRequestModel.user_full_name_user_id == user_id,
            LeaveRequestModel.is_active == True).all()
        logger.info('Leave data search success {}'.format(data))
        return data

    except Exception as error:
        logger.error('Leave_data_search error {}'.format(error))
    return data


def leave_appy_to_search_service(user_id):
    """
        leave search service is search all data from leave apply table
        :return: if data in table >>> return data in list,
                 if data not in table >>> return empty list,
                 if error get in search service >>> return None
        """

    logger.info("{}".format(leave_search_service.__doc__))

    data = None
    try:
        data = LeaveRequestModel.query.filter(
            LeaveRequestModel.leave_apply_to_admin_id == user_id,
            LeaveRequestModel.is_active == True).all()
        logger.info('Leave data search success {}'.format(data))
        return data

    except Exception as error:
        logger.error('Leave_data_search error {}'.format(error))
    return data


def leave_admin_all_data_search_service(full_name):
    """
        leave_admin_all_data_search_service is search all
         data from category table
        :return: if data in table >>> return data in list,
                 if data not in table >>> return empty list,
                 if error get in search service >>> return None
        """

    logger.info("{}".format(leave_admin_all_data_search_service.__doc__))

    data = None
    try:
        data = LeaveRequestModel.query.filter(
            LeaveRequestModel.leave_apply_to == full_name,
            LeaveRequestModel.is_active == True).all()
        logger.info('Leave data search success {}'.format(data))
        return data

    except Exception as error:
        logger.error('Leave_data_search error {}'.format(error))
    return data


def leave_check_leave_id(leave_request_id):
    """
        leave_check_leave_request_id is use for check that a login_id is in
        data base or not.

        :param leave_request_id: leave_request_id
        :type: integer
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
    """

    logger.info("{}".format(leave_request_id.__doc__))

    user = []
    try:
        user = LeaveRequestModel.query.filter(
            LeaveRequestModel.leave_request_id ==
            leave_request_id).first()
        logger.info('leave_check_leave_id is {}'.format(user))
    except Exception as error:
        logger.error('leave_check_leave_id error {}'.format(error))
    return user


def leave_user_update_service(user):
    """
        user update service is merge data in user table for update
        data in user table.

        :param id, full_name : leave_model object

    """
    logger.info("{}".format(leave_user_update_service.__doc__))

    try:
        db.session.merge(user)
        db.session.commit()
        logger.info(
            'login_delete success{}'.format(user))

    except Exception as error:
        logger.error('leave_model error{}'.format(error))


def leave_delete_service(leave_model):
    """
        login delete service is merge data in login table for soft user
        delete in login table.

        :param leave_model: leave_model object

    """

    logger.info("{}".format(leave_delete_service.__doc__))

    try:
        db.session.merge(leave_model)
        db.session.commit()
        logger.info(
            'login_delete success{}'.format(leave_model.leave_request_id))

    except Exception as error:
        logger.error('login_delete error {}'.format(error))


def leave_search_dash_service():
    """
        category search service is search all data from category table
        :return: if data in table >>> return data in list,
                 if data not in table >>> return empty list,
                 if error get in search service >>> return None
        """

    logger.info("{}".format(leave_search_service.__doc__))

    data = None
    try:
        data = LeaveRequestModel.query.filter(
            LeaveRequestModel.is_active == True,
            LeaveRequestModel.leave_status == "Accepted").all()
        logger.info('Leave data search success {}'.format(data))
        return data

    except Exception as error:
        logger.error('Leave_data_search error {}'.format(error))
    return data
