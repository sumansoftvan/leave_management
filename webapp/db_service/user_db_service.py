from logger_config import MyLogger
from webapp import db
from webapp.models.login_details_model import LoginDetailsModel
from webapp.models.user_details_model import UserDetailsModel

logger = MyLogger().get_logger("user_details_db_logger")

"""user details db service gives a data base query regarding to user details 
table like insert, search, delete and update"""


def user_details_insert_service(user_details_model):
    """
        user_details_insert_service is insert data into user details table
        :param user_details_model: user model object

    """
    logger.info("{}".format(user_details_insert_service.__doc__))

    try:
        db.session.add(user_details_model)
        db.session.commit()
        logger.info('user_details_insert_service success {}'.format(
            user_details_model))

    except Exception as error:
        logger.error('user_insert error {}'.format(error))


def user_active_search_service():
    """
        user_search_service is search all data from table (active status true)
        :return: if data in table >>> return data in list,
                 if data not in table >>> return empty list,
                 if error get in search service >>> return None
    """

    logger.info("{}".format(user_active_search_service.__doc__))
    data = None
    try:
        data = UserDetailsModel.query.filter(UserDetailsModel.is_active ==
                                             True).all()
        logger.info('user_active_search_service success {}'.format(data))

    except Exception as error:
        logger.error('user_active_search_service error {}'.format(error))
    return data


def user_delete_service(user_model):
    """
        user delete service is merge data in user table for soft user
        delete in user table.

        :param user_model: user_model object

    """

    logger.info("{}".format(user_delete_service.__doc__))

    try:
        db.session.merge(user_model)
        db.session.commit()
        db.session.close()
        logger.info('user_delete success {}'.format(user_model.user_id))

    except Exception as error:
        logger.error('user_delete error{}'.format(error))


def user_update_service(user_model):
    """
        user update service is merge data in user table for update
        data in user table.

        :param user_model: user model object

    """
    logger.info("{}".format(user_update_service.__doc__))

    try:
        db.session.merge(user_model)
        db.session.commit()
        db.session.close()
        logger.info('user_update success{}'.format(user_model.user_id))

    except Exception as error:
        logger.error('user_update error{}'.format(error))


def user_search_service(user_login_id):
    """
        user_search_service is use for check that a login_id is in
        data base or not.

        :param user_login_id: user_login_id
        :type: integer
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
    """

    logger.info("{}".format(user_search_service.__doc__))

    user = []
    try:
        user = UserDetailsModel.query.filter(UserDetailsModel.user_login_id ==
                                             user_login_id).first()
        logger.info('user_search_service is {}'.format(user))
    except Exception as error:
        logger.error('user_search_service error {}'.format(error))
    return user


def user_full_name_service(login_id):
    """
        user_full_name_service is use for check that a login_id is in
        data base or not.

        :param login_id: login_id
        :type: integer
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
    """

    logger.info("{}".format(user_full_name_service.__doc__))

    user = []
    try:
        user = UserDetailsModel.query.filter(
            UserDetailsModel.user_login_id ==
            login_id).first()
        logger.info('user_full_name_service is {}'.format(user))
    except Exception as error:
        logger.error('user_full_name_service error {}'.format(error))
    return user


def user_full_name_service_user_id(user_id):
    """
        user_full_name_service is use for check that a login_id is in
        data base or not.

        :param login_id: login_id
        :type: integer
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
    """

    logger.info("{}".format(user_full_name_service.__doc__))

    user = []
    try:
        user = UserDetailsModel.query.filter(
            UserDetailsModel.user_id ==
            user_id).first()
        logger.info('user_full_name_service is {}'.format(user))
    except Exception as error:
        logger.error('user_full_name_service error {}'.format(error))
    return user


def user_check_user_full_name(full_name):
    """
        user_check_user_id is use for check that a login_id is in
        data base or not.

        :param full_name: full_name
        :type: integer
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
    """

    logger.info("{}".format(user_check_user_full_name.__doc__))

    user = []
    try:
        user = UserDetailsModel.query.filter(UserDetailsModel.user_full_name ==
                                             full_name).first()
        logger.info('user_check_user_full_name is {}'.format(user))
    except Exception as error:
        logger.error('user_check_user_full_name error {}'.format(error))
    return user


def user_login_use_check(login_username):
    """
        user_login_use_check is use for check that a login_username &
        user_full_name is in data base or not.

        :param login_username: login_username, user_full_name
        :type: string
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
        """

    logger.info("{}".format(user_login_use_check.__doc__))

    user = []
    try:
        login_details = LoginDetailsModel.query.filter(
            LoginDetailsModel.login_username ==
            login_username).first()

        if login_details is None:
            user = None

        if login_details is not None:
            user = "login_user_name Already use."
            return user

    except Exception as error:
        logger.error('user_login_use_check check error {}'.format(error))
    return user


def user_name_login_use_check(user_full_name):
    """
        user_login_use_check is use for check that a login_username &
        user_full_name is in data base or not.

        :param login_username: login_username, user_full_name
        :type: string
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
        """

    logger.info("{}".format(user_login_use_check.__doc__))

    user = []
    try:
        user_details = UserDetailsModel.query.filter(
            UserDetailsModel.user_full_name ==
            user_full_name).first()

        if user_details is None:
            user = None

        if user_details is not None:
            user = "user_full_name Already use."

    except Exception as error:
        logger.error('user_login_use_check check error {}'.format(error))
    return user


def user_login_use_check_new_user(login_username, user_full_name):
    """
        user_login_use_check is use for check that a login_username &
        user_full_name is in data base or not.

        :param login_usernameTPUucCmvJWUBVB7TnYqJ, user_full_name: login_username, user_full_name
        :type: string
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
        """

    logger.info("{}".format(user_login_use_check.__doc__))

    user = []
    try:
        login_details = LoginDetailsModel.query.filter(
            LoginDetailsModel.login_username ==
            login_username).first()

        user_details = UserDetailsModel.query.filter(
            UserDetailsModel.user_full_name ==
            user_full_name).first()

        if login_details is None and user_details is None:
            user = None

        if login_details is not None:
            user = 'login_user_name Already use.'
            return user

        if user_details is not None:
            user = 'user_full_name Already use.'

    except Exception as error:
        logger.error('user_login_use_check check error {}'.format(error))
    return user


def user_cl_pl_get(user_id):
    """
        user_full_name_service is use for check that a login_id is in
        data base or not.

        :param login_id: login_id
        :type: integer
        :return: if present in database >>> class category, EX:- <LoginModel 1>
                 if not present in database >>> None
                 if facing error in check >>  empty list
    """

    logger.info("{}".format(user_full_name_service.__doc__))

    user = []
    try:
        user = UserDetailsModel.query.filter(
            UserDetailsModel.user_id ==
            user_id).first()
        logger.info('user_full_name_service is {}'.format(user))
    except Exception as error:
        logger.error('user_full_name_service error {}'.format(error))
    return user