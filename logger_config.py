import logging
import os
import datetime

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
FORMATTER = logging.Formatter(
    "%(asctime)s - %(levelname)s -"
    " %(filename)s:%(lineno)s - "
    "%(funcName)2s() : %(message)s")
LOG_FILE_PATH = basedir + "/logs"
LOG_FILE_NAME = "{}.log".format(datetime.date.today())


class MyLogger:
    def __init__(self):
        pass

    @staticmethod
    def get_logger(logger_name):
        """
        This function is used to create logger.

        :param logger_name: Name of the logger
        :type logger_name: String
        :return: logger object
        :rtype: object
        """
        if not os.path.exists(LOG_FILE_PATH):
            try:
                import time
                os.makedirs(LOG_FILE_PATH)
                os.chmod(LOG_FILE_PATH, 0o777)
                time.sleep(2)
            except OSError:
                pass
        log_level = os.environ.get('LOGLEVEL', 'INFO').upper()
        logging.basicConfig(level=log_level)
        logger = logging.getLogger(logger_name)
        logger.setLevel(log_level)
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(FORMATTER)
        logger.addHandler(stream_handler)
        f_handler = logging.FileHandler(LOG_FILE_PATH + "/" + LOG_FILE_NAME)
        f_handler.setFormatter(FORMATTER)
        f_handler.setLevel(log_level)
        logger.addHandler(f_handler)
        return logger

