from webapp.utils import app_config
from webapp import app
import webapp.models


if __name__ == '__main__':
    app.run(threaded=app_config.threaded, debug=app_config.debug,
            port=app_config.port)

