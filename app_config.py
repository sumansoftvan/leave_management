import argparse
from configparser import ConfigParser

config = ConfigParser()
parser = argparse.ArgumentParser()
parser.add_argument("run_env")
args = parser.parse_args()
env = args.run_env

if env == "dev":
    config.read('/home/dev6/PycharmProjects/demo/config/dev_config.ini')
if env == "prod":
    config.read('/home/dev6/PycharmProjects/demo/config/prod_config.ini')
if env == "qa":
    config.read('/home/dev6/PycharmProjects/demo/config/qa_config.ini')

data_base = config.get('flask', 'database')
secret_key = config.get('flask', 'secret_key')
debug = config.getboolean('flask', 'debug')
port = config.getint('flask', 'port')
threaded = config.getboolean('flask', 'threaded')
token_expire = config.getint('jwt', 'token_expiration_time')
token_algorithm = config.get('jwt', 'token_algorithm')
